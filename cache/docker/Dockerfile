FROM ubuntu:19.04

ENV PYTHONUNBUFFERED=1
ENV USER_=themis
ENV HOME="/home/${USER_}"

ARG UID=1000
ARG GID=1000

RUN groupadd --system --gid "${GID}" "${USER_}" \
    && useradd --system --create-home --shell /bin/bash --home-dir "${HOME}" \
        --uid "${UID}" --gid "${USER_}" "${USER_}"

WORKDIR "${HOME}"

COPY install_marvin.sh marvin /opt/marvin/

COPY requirements /opt/requirements/

RUN set -eux \
    && apt-get update \
    && apt-get install --yes --only-upgrade openssl ca-certificates \
    && apt-get install --yes \
        build-essential \
        default-jre \
        libopenbabel-dev \
        pkg-config \
        python3-dev \
        python3-pip \
        swig \
    && /opt/marvin/install_marvin.sh "${HOME}/.chemaxon" \
    && pip3 install --upgrade pip setuptools wheel \
    && pip3 install -r /opt/requirements/requirements.txt \
    && chown -R "${USER_}:${USER_}" "${HOME}" \
# Cleaning up will make the image size smaller.
    && apt-get remove --purge --yes build-essential \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /root/.cache /tmp/* /var/tmp/*

VOLUME ["${HOME}/data", "${HOME}/data/compounds", "${HOME}/data/metanetx", "${HOME}/data/quilt"]

COPY Makefile ./

RUN chown -R "${USER_}:${USER_}" "${HOME}"

USER "${USER_}"

CMD ["bash"]
