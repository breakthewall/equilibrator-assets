"""Tests for ChemAxon."""
# The MIT License (MIT)
#
# Copyright (c) 2020 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import pytest


try:
    from openbabel import openbabel

    assert openbabel

    from equilibrator_assets.chemaxon import verify_cxcalc

except ImportError:
    # TODO: also verify that the OpenBabel version is the latest one
    pytest.skip("Cannot test without OpenBabel.", allow_module_level=True)

from equilibrator_assets.generate_compound import get_or_create_compound


def test_get_from_inchis(comp_contribution) -> None:
    """Create a small compound table for testing ChemAxon and OpenBabel."""
    inchis = [  # adenine
        "InChI=1S/C5H5N5/c6-4-3-5(9-1-7-3)10-2-8-4/h1-2H,(H3,6,7,8,9,10)",
        "InChI=1S/C2H4O2/c1-2(3)4/h1H3,(H,3,4)/p-1",  # acetate
        "InChI=1S/C6H6/c1-2-4-6-5-3-1/h1-6H",  # benzene
    ]
    compound_list = get_or_create_compound(
        comp_contribution.ccache, inchis, mol_format="inchi"
    )

    assert compound_list[0].dissociation_constants == [9.84, 2.51]
    assert compound_list[1].dissociation_constants == [4.54]
    assert compound_list[2].dissociation_constants == []

    assert len(compound_list[0].magnesium_dissociation_constants) == 2
    assert len(compound_list[1].magnesium_dissociation_constants) == 1
    assert len(compound_list[2].magnesium_dissociation_constants) == 0


def test_create_from_smiles(comp_contribution) -> None:
    """Create a compound that is not in the cache."""
    if not verify_cxcalc():
        pytest.skip("cannot perform this test since cxcalc is not installed.")

    smiles = [
        "OCC(N)C(O)CO",  # 3-Aminobutane-1,2,4-triol
        "CCCOP(=O)(O)O",  # propyl-phosphate
    ]
    compound_list = get_or_create_compound(
        comp_contribution.ccache,
        smiles,
        mol_format="smiles",
    )

    assert compound_list[0].dissociation_constants == [13.69, 8.92]
    assert compound_list[1].dissociation_constants == [6.84, 1.82]
    for c in compound_list:
        assert c.id < 0
        assert len(c.magnesium_dissociation_constants) == 0
        assert c.group_vector is not None
